﻿using Api.Core;
using Api.DBContext;
using Api.Dtos;
using Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MenuController : Controller
    {
        private readonly ApiDbContext _dbContext;
        public MenuController(ApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        [HttpGet]
        [Route("allmenus")]
        public async Task<List<Menu>> AllMenus()
        {
            var result = await _dbContext.Menus.ToListAsync();
            return result;
        }

        [HttpGet]
        [Route("menu/{id}")]
        public async Task<Menu> GetMenuById(int id)
        {
            var result = await _dbContext.Menus.FirstOrDefaultAsync(x=> x.Id == id);
            return result;
        }

        [HttpPost]
        [Route("addmenu")]
        public async Task<Success> AddMenu(MenuDto menu)
        {
            var Menu = new Menu
            {
                Name = menu.Name,
                Description = menu.Description
            };
            await _dbContext.Menus.AddAsync(Menu);
            await _dbContext.SaveChangesAsync();
            return new Success()
            {
                status = 200,
                data = ""
            };
        }

        [HttpDelete("{id}")]
        public async Task<Success> Delete(string id)
        {
            var result = await _dbContext.Menus.FindAsync(id);
             _dbContext.Remove(result);
            await _dbContext.SaveChangesAsync();
            return new Success()
            {
                status = 200,
                data = ""
            };
        }
    }
}
