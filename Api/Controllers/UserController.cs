﻿using Api.Core;
using Api.DBContext;
using Api.Dtos;
using Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly ApiDbContext _dbContext;
        public UserController(ApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        [Route("login")]
        public async Task<Success> Login(UserDto user)
        {
            var result = _dbContext.Users.Where(x=> x.Username == user.Username && x.Password == user.Password).FirstOrDefault();
            if(result == null)
            {
                throw new Exception("Tài khoản or mật khẩu không đúng !!!");
            }
            else
            {
                return new Success()
                {
                    status = 200,
                    data = ""
                };
            }    
        }

        [HttpPost]
        [Route("createuser")]
        public async Task<Success> CreateUser(UserDto user)
        {
            var result = new User
            {
                Username = user.Username,
                Password = user.Password
            };
            await _dbContext.Users.AddAsync(result);
            await _dbContext.SaveChangesAsync();
            return new Success()
            {
                status = 200,
                data = ""
            };
        }
    }
}
