﻿using Api.Core;
using Api.DBContext;
using Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImageController : Controller
    {
        private readonly ApiDbContext _dbContext;
        public ImageController(ApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        [Route("allimages")]
        public async Task<List<Image>> AllMenus()
        {
            var result = await _dbContext.Images.ToListAsync();
            return result;
        }

        [HttpGet]
        [Route("images/{menuid}")]
        public async Task<List<Image>> ImagesByMenuId(int menuid)
        {
            var result = await _dbContext.Images.Where(x=> x.IdMenu == menuid).ToListAsync();
            return result;
        }

        [HttpPost]
        [Route("upload")]
        public async Task<Success> UpLoadImages(List<IFormFile> files,int idmenu)
        {
            foreach (var file in files)
            {
                var basePath = Path.Combine(Directory.GetCurrentDirectory() + "\\Images\\");
                bool basePathExists = System.IO.Directory.Exists(basePath);
                if (!basePathExists) Directory.CreateDirectory(basePath);
                // var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var filePath = Path.Combine(basePath, file.FileName);
                // var extension = Path.GetExtension(file.FileName);
                if (!System.IO.File.Exists(filePath))
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                    var imageModel = new Image
                    {                                             
                        Name = file.FileName,
                        IdMenu = idmenu

                    };
                    await _dbContext.Images.AddAsync(imageModel);
                    await _dbContext.SaveChangesAsync();
                }
            }
            return new Success() { status = 200, data = "" };
        }

        [HttpGet]
        [Route("download/{id}")]
        public async Task<IActionResult> DownloadIamge(int id)
        {
            var file = await _dbContext.Images.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (file == null) return null;

            var basePath = Path.Combine(Directory.GetCurrentDirectory() + "\\Images\\");
            var filePath = Path.Combine(basePath, file.Name);

            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, "application/vnd",file.Name);
        }

        [HttpDelete("delete/{id}")]
        public async Task<Success> DeleteFile(int id)
        {
            var file = await _dbContext.Images.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (file == null) return null;        
            var basePath = Path.Combine(Directory.GetCurrentDirectory() + "\\Images\\");
            var filePath = Path.Combine(basePath, file.Name);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            _dbContext.Images.Remove(file);
            _dbContext.SaveChanges();
            return new Success()
            {
                status = 200,
                data = ""
            };
        }
    }
}
